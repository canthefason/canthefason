package canthefason

class MediaController {

    def picture() {

        session.setAttribute('fasonMode', true)
        render (view: "index", model: [dataType:'picture', title: 'Gallery', fasonMode: true])
    }

    def performance() {
        session.setAttribute('fasonMode', true)
        render (view: "index", model: [dataType:'performance', title: 'Performance', fasonMode: true])
    }


}
