package canthefason

import org.apache.commons.lang.StringUtils

class IndexController {

    def index() {
        def mode = request.getParameter("mode")
        def fasonMode = false
        if (mode == "fason") {
            fasonMode = true;
            session.setAttribute('fasonMode', true)
        } else if (mode =="can") {
            fasonMode = false;
            session.setAttribute('fasonMode', false)
        } else if (StringUtils.isBlank(mode)) {
            fasonMode = session.getAttribute('fasonMode') ? session.getAttribute('fasonMode') : false
        }


        [fasonMode: fasonMode, mode: mode]
    }

    def change() {
        session.setAttribute('fasonMode', !session.getAttribute('fasonMode'))
        def mode = session.getAttribute('fasonMode') ? "fason" : "can"
        redirect (action: "index", params: ["mode": mode])
    }

    def topBar() {
        def fasonMode = session.getAttribute('fasonMode') ? session.getAttribute('fasonMode') : false
        [fasonMode: fasonMode]
    }

    def footer() {
        def text = session.getAttribute('fasonMode') ? "Singing & Guitar Playing Muppet" : "Agile Software Developer"
        [text: text]
    }

    def underConst() {
        def fasonMode = session.getAttribute('fasonMode') ? session.getAttribute('fasonMode') : false
        [fasonMode: fasonMode]
    }

    def resume() {
        session.setAttribute('fasonMode', false)
        [fasonMode: false]
    }

}
