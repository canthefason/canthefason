package canthefason

class ContactController {

    def mailService

    def index() {
        def command = new ContactCommand()
        [command: command, fasonMode: session.getAttribute("fasonMode")]
    }

    def post(ContactCommand command) {

        if (command.validate()) {
            def renderedMail = g.render(template: 'message', model: [command: command])
            mailService.sendMail {
                to 'can.yucel@gmail.com'
                from command.email
                subject 'Mesaj var, bak bi'
                html renderedMail
            }

            flash.message = 'Message successfully sent'
            command = new ContactCommand()
        }

        render view: 'index', model: [command: command]
    }
}

class ContactCommand {
    String fullname
    String email
    String message

    static constraints = {
        fullname blank: false
        email blank: false, email: true
        message blank: false
    }
}
