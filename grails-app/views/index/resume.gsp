<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html >

<head>
    <meta name='layout' content='main'/>
    <g:render template="title"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'light.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'font-awesome.css')}" type="text/css">


</head>
<body>

<div class="container-fluid" style="margin-top: 40px;">
    <div class="row-fluid">
        <div class="profile-block span12">
            <div class="span6 engulf" id="profile">
                <div class="profile-img pull-left">
                    <img src="${resource(dir: "images", file: "can-efendi.png")}" alt="profile picture"/>
                </div>
                <div class="profile-description pull-left">
                    <h1>Can Yücel</h1>
                    <h3>Agile Software Developer</h3>
                    <div class="contact-details">
                        <i class="icon-map-marker"></i> Istanbul, Turkey | MSc Software Engineer <br/>
                        <i class="icon-inbox"> </i> <a href="#contactModel" data-toggle="modal">  <g:message code="contact.email"/></a><br/>
                        <i class="icon-phone-sign"> </i>   <g:message code="contact.phone"/> <br/>
                        <i class="icon-globe"> </i>  <a href="http://www.canthefason.com">www.canthefason.com</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="timeline">
            <div class="experience">
                <div class="heading alert alert-info">
                    <i class=" icon-briefcase"></i> Work experience
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "n11-cv-logo.png")}" class="pull-left" width="100" height="100" alt="company logo" />
                    <span class="bold">February 2013 - March 2013</span>
                    Software Developer at n11.com | Istanbul / TURKEY     <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>
                                <a href="http://www.n11.com">n11.com</a> <br/>
                                Mallfront development (Java, Spring MVC)
                            </li>
                        </ol>
                    </span>
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "granul-cv-logo.png")}" class="pull-left" alt="company logo" />
                    <span class="bold">May 2012  - January 2013</span>
                    Software Developer at Granul Technology | Istanbul / TURKEY     <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>
                                <a href="http://www.n11.com" target="_blank">n11.com</a> <br/> Development of REST Services (JAX-RS) and Search API in mallfront.
                            </li>
                            <li>
                                <a href="http://gollercepte.tv" target="_blank">GollerCepte.tv</a> <br/> FE – BE integration for the smart phone and Ipad environments. (ASP .NET)<br/>
                            </li>
                            <li>
                                <a href="http://www.guzelinsan.org" target="_blank">GuzelInsan.org</a> <br/> Development and system administration of the project. (Grails)
                            </li>
                        </ol>
                    </span>
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "tart-cv-logo.png")}" class="pull-left" alt="company logo"/>
                    <span class="bold">February 2011 - May 2012</span>
                    Lead Backend Developer at Tart New Media | Istanbul / TURKEY <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>
                                <a href="http://www.playstore.com" target="_blank">Playstore.com</a> <br/>
                                Responsible for the architectural design and development. <br/>
                                Build management by Jenkins and Git. <br/>
                                Implementation of indexing mechanism via Apache SOLR. <br/>
                                Development of an SQL generator, based on Doctrine 2.0 entities. (PHP, Zend Framework)
                            </li>
                            <li>
                                <a href="http://www.keyfimoda.com" target="_blank">Keyfimoda.com</a> <br/>
                                Development of SOAP web services. (PHP, Zend Framework)
                            </li>
                        </ol>
                    </span>
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "tubitak-cv-logo.png")}" class="pull-left" alt="company logo" />
                    <span class="bold">October 2009 - January 2011 </span>
                    Frontend Developer at TÜBİTAK | Kocaeli / TURKEY   <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>
                                DATAS (Submarine Tactics Simulator)<br/>
                                Development of graphical user interface and plugins of the project. (C++, Qt)
                            </li>
                        </ol>
                    </span>
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "sabanci-cv-logo.png")}" class="pull-left" alt="company logo" />
                    <span class="bold">September 2007 - June 2009 </span>
                    Teaching Assistant at Sabancı University  | Istanbul / TURKEY <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>Software Engineering (Spring 2009)<br/>
                            Advanced Programming (Spring 2008)<br/>
                            Introduction to Computing (Fall 2007 & Fall 2008)</li>
                        </ol>
                    </span>
                </div>
                <div class="list-block ">
                    <img src="${resource(dir: "images", file: "argefar-cv-logo.png")}" class="pull-left" alt="company logo" />
                    <span class="bold">July 2004 - June 2007</span>
                    Supervisor at Argefar  | Izmir / TURKEY <br/>
                    <span class="more-info">
                        <ol>
                            <li></li>
                            <li>Responsible for the management of the IT department. (Part-time)</li>
                        </ol>
                    </span>
                </div>
            </div>
            <div class="education">
                <div class="heading alert alert-info">
                    <i class=" icon-book"></i> Education
                </div>
                <div class="list-block ">
                    <span class="bold">Sabancı University</span>
                    MS, Computer Science - Computer & Network Security Program<br/>
                    <ol>
                        <li></li>
                        <li>
                            Thesis: A Secure Micropayment Scheme in Wireless Mesh Networks <br/>
                            Advisor: Assoc. Prof. Dr. Albert Levi
                        </li>
                    </ol>
                    2007 -  2010
                </div>
                <div class="list-block ">
                    <span class="bold">Ege University</span>
                    BS, Computer Engineering <br/>
                    <ol>
                        <li></li>
                        <li>
                            Thesis: Simulation of a Congestion Control Algorithm <br/>
                            Advisor: Prof. Dr. Levent Toker
                        </li>
                    </ol>
                    2003 -  2007
                </div>
                <div class="list-block ">
                    <span class="bold">Porto University</span>
                    Erasmus, Informatics and Computer Engineering <br/>
                    2005 - 2006
                </div>
            </div>
            <div class="awards">
                <div class="heading alert alert-info">
                    <i class="icon-trophy"></i> Honors and Awards </div>
                <div class="list-block">
                    <ol>
                        <li>Sabancı University MS. Full Scholarship, 2007-2009</li>
                        <li>TÜBİTAK MS. Scholarship, 2007-2009</li>
                    </ol>
                </div>
            </div>
            <div class="skillset">
                <div class="heading alert alert-info">
                    <i class="icon-wrench"></i> Skills & Expertise
                </div>
                <div class="list-block ">
                    <ol>
                        <li>
                            Java, Javascript, Groovy, PHP, C++, C#, Scala
                        </li>
                        <li>
                            Grails, Spring MVC, Zend Framework, JQuery, Qt, OMNET++
                        </li>
                        <li>
                            Git, Solr, Hibernate, Doctrine, RabbitMQ, Web Services
                        </li>
                        <li>
                            Design Patterns, OOP, Jenkins, TDD, MongoDB, PlantUML
                        </li>
                    </ol>
                </div>
            </div>
            <div class="interests">
                <div class="heading alert alert-info">
                    <i class="icon-music"></i> Interests </div>
                <div class="list-block">
                    <ol>
                        <li><g:link action="index" params="[mode:'fason']">Music</g:link></li>
                        <li>Snowboard</li>
                    </ol>
                </div>
            </div>
            <div class="social-links">
                <ul class="quicklinks">
                    <li><a href="<g:message code="contact.linkedin"/>" target="_blank"><i class="icon-linkedin-sign icon-large"></i></a></li>
                    <li><a href="<g:message code="contact.facebook"/>" target="_blank"><i class="icon-facebook-sign icon-large"></i></a></li>
                </ul>
            </div>
        </div>
    </div><!--/row-->

</div><!--/.fluid-container-->


<!-- contact model -->
<div id="contactModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel"><i class="icon-envelope-alt"></i> Contact Can Yücel</h3>
    </div>
    <g:form action="post" controller="contact"  class="">
        <div class="modal-body">
            <input type="text" name="fullname" placeholder="Name"> <br/>
            <input type="text" name="email" placeholder="Email"> <br/>
            <textarea name="message" placeholder="Your Message"></textarea>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary pull-left">Send Email</button>
        </div>
    </g:form>
</div>
</body>
</html>