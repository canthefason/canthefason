<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <g:if test="${!fasonMode}">
        <title>Can Yucel - Agile Software Developer</title>
    </g:if>
    <g:else>
        <title>Can the Fason - Singer & Guitar Player</title>
    </g:else>
    <meta name='layout' content='main'/>
    <meta content="">
</head>
<body>

<div class="container" style="margin-top: 20px;">
    <div class="row testimonials">
        <div class="span12">
            <g:if test="${!fasonMode}">
                <img src="${resource(dir: 'images', file: 'under-construction.png')}" alt="placeholder+image">
            </g:if>
            <g:else>
                <img src="${resource(dir: 'images', file: 'munder.jpeg')}" alt="placeholder+image">
            </g:else>
        </div>
    </div>
</div>

</body>
</html>