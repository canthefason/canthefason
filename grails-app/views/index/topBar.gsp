<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <g:link class="brand" controller="index" action="change">
                <g:if test="${!fasonMode}">
                    <img src="${resource(dir: 'images', file: 'can-on-thefason.png')}" width="156" alt="Canthefason - Senior Software Engineer">
                </g:if>
                <g:else>
                    <img src="${resource(dir: 'images', file: 'canthefason-on.png')}" width="156" alt="cantheFason - Junior Musician">
                </g:else>
            </g:link>
            <a class="brand" href="index.html" style="display:none;"><img src="${resource(dir: 'images', file: 'canthefason-on.png')}" width="156" alt="Typica - Bootstrap Awesome Template!"></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">
                    <li class="active"><g:link uri="/">Home</g:link></li>
                    <g:if test="${!fasonMode}">
                        <li><g:link action="resume">CV/Resume</g:link></li>
                    </g:if>
                    <g:else>
                        <li><g:link action="picture" controller="media">Gallery</g:link></li>
                        <li><g:link action="performance" controller="media">Performance</g:link></li>
                    </g:else>
                    <li><g:link controller="contact">Contact</g:link></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>