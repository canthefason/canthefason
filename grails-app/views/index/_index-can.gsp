<div class="slider-block" xmlns="http://www.w3.org/1999/html">

    <div id="myCarousel" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="active item">
                <img src="${resource(dir: 'images', file: 'slide_bg2.jpeg')}" alt="Someimage">
                <div class="caption bottom-left container">
                    <p class="medium white text-pink">Agile Software Developer</p>
                </div>
            </div>
        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>

</div>

<div class="hero-unit">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h1>In development we <strong class="pink">trust</strong></h1>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <!-- TESTIMONIALS 3col -->
    <div class="row testimonials">
        <div class="span4">
            <img class="border" src="${resource(dir: 'images', file: 'n11-logolu.png')}" alt="Numara 11">
            <a href="http://www.n11.com" target="_blank"> <h2>Numara 11</h2></a>
            <p>B2C E-Commerce project. <br/>(Financed by Dogus Planet)</p>
        </div>
        <div class="span4">
            <img src="${resource(dir: 'images', file: 'playstore.png')}" alt="Playstore">
            <a href="http://www.playstore.com" target="_blank"> <h2>Playstore</h2></a>
            <p>Steam like gaming store. <br/> (Financed by TTnet Inc.)</p>
            %{--<p>Backend software design and implementation of TTnet Inc. gaming store</p>--}%
        </div>
        <div class="span4">
            <img src="${resource(dir: 'images', file: 'denizalti.png')}" alt="Datas">
            <h2>DATAS</h2>
            <p>Submarine Tactics Simulator. <br/>(Financed by Turkish Naval Forces)</p>
        </div>
    </div>

    <div class="section-title">
        <h2>Some other projects still in progress/already finished. </h2>
    </div>

    <!-- TESTIMONIALS 4col -->
    <div class="row testimonials">
        <div class="span3">
            <img class="border" src="${resource(dir: 'images', file: 'guzel-insan.png')}" alt="Guzel Insan">
            <h2>Guzel Insan</h2>
            <p>Social scholarship platform. </p>
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'modakeyfi.png')}" alt="Keyfimoda / Modajenik">
            <h2>Keyfimoda / Modajenik</h2>
            %{--<p>Third party integration via SOAP web services</p>--}%
            <p>Private shopping platforms.</p>
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'turkcell-goller-cepte.png')}" alt="Goller Cepte">
            <h2>Goller Cepte</h2>
            %{--<p>Frontend-Backend integration of Turkcell service.</p>--}%
            <p>Turkcell wap service.</p>
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'tuttur-logo.png')}" alt="Tuttur">
            <h2>Tuttur.com</h2>
            <p>Online betting system.</p>
        </div>
    </div>
</div><!-- .container -->
