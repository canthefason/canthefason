<div class="slider-block">

    <div id="myCarousel" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="active item">
                <img src="${resource(dir: 'images', file: 'mayhem.jpeg')}" alt="Someimage">
                <div class="caption bottom-left container">

                    <p class="medium white text-pink">Singer & Guitar Player</p>
                </div>
            </div>
        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>

</div>

<div class="hero-unit">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h1>In Music we <strong class="pink">trust</strong></h1>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <!-- TESTIMONIALS 3col -->
    <div class="row-fluid testimonials">
        <div class="span3"></div>
        <div class="span6">
            <a href="http://www.facebook.com/cipetpetband" target="_blank">
                <img src="${resource(dir: 'images', file: 'cipetpet.png')}" alt="Çipetpet Indie Rock"/>
                <h1>Çipetpet</h1>
            </a>
        </div>
        <div class="span3"></div>
    </div>
    <div class="row testimonials">

        <div class="span4">
            <a href="http://www.facebook.com/NeseliGunlerKumpanyasi" target="_blank">
                <img src="${resource(dir: 'images', file: 'neseli.png')}" alt="Neşeli Günler Turkish Pop"/>
                <h2>Neşeli Günler Kumpanyası</h2>
            </a>
        </div>
        <div class="span4">
            <a href="http://www.facebook.com/pages/Müzikus-Çok-Sesli-Pop-ve-Rock-KoroSU/121372344550221">
                <img src="${resource(dir: 'images', file: 'korosu.png')}" alt="Sabanci University Pop & Rock Choir"/>
                <h2>Sabancı University Pop &amp Rock Choir</h2>
            </a>
        </div>
        <div class="span4">
            <a href="http://www.facebook.com/pages/Shuffle/167540263993" target="_blank">
                <img src="${resource(dir: 'images', file: 'shuffle.png')}" alt="Shuffle Alternative Pop/Rock"/>
                <h2>Shuffle</h2>
            </a>
        </div>


    </div>


    <div class="section-title">
        <h2>Also known as</h2>
    </div>

    <!-- TESTIMONIALS 4col -->
    <div class="row testimonials">
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'kermit_bey.png')}" title="Kermit the Frog" alt="Kermit the Frog">
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'frodo_bey.png')}" title="Frodo the Ring Bearer">
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'kirpik_bey.png')}" title="Oscar the Grouch">
        </div>
        <div class="span3">
            <img src="${resource(dir: 'images', file: 'krtek_bey.png')}" title="Krtek the Mole">
        </div>
    </div>
</div><!-- .container -->

%{--<div class="hero-unit">--}%
%{--<div class="container">--}%
%{--<div class="row">--}%
%{--<div class="span12">--}%
%{--<h1>Who are we?</h1>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%

%{--<div class="container">--}%
%{--<div class="team">--}%

%{--<div class="row">--}%

%{--<div class="span4 section-title horizontal-left">--}%
%{--<h2>Team members <i class="icon-chevron-right"></i></h2>--}%
%{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}%
%{--tempor incididunt ut labore et dolore magna aliqua.--}%
%{--<div class="btn btn-large pull-right">Know more &raquo;</div></p>--}%
%{--</div>--}%

%{--<div class="span8">--}%
%{--<div class="row">--}%
%{--<div class="span2 item">--}%
%{--<div class="circle-thumbnail">--}%
%{--<img src="${resource(dir: 'images', file: '1.jpeg')}" alt="team member">--}%
%{--</div>--}%
%{--<h2>Kathy Jackson</h2>--}%
%{--<p>Incididunt ut labore et dolore magna aliqua.</p>--}%
%{--</div>--}%
%{--<div class="span2 item">--}%
%{--<div class="circle-thumbnail">--}%
%{--<img src="${resource(dir: 'images', file: '2.jpeg')}" alt="team member">--}%
%{--</div>--}%
%{--<h2>Erin Fields</h2>--}%
%{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}%
%{--tempor. </p>--}%
%{--</div>--}%
%{--<div class="span2 item">--}%
%{--<div class="circle-thumbnail">--}%
%{--<img src="${resource(dir: 'images', file: '3.jpeg')}" alt="team member">--}%
%{--</div>--}%
%{--<h2>Steven Jones</h2>--}%
%{--<p>Ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>--}%
%{--</div>--}%
%{--<div class="span2 item">--}%
%{--<div class="circle-thumbnail">--}%
%{--<img src="${resource(dir: 'images', file: '4.jpeg')}" alt="team member">--}%
%{--</div>--}%
%{--<h2>Jan Kowalski</h2>--}%
%{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%
%{--</div>--}%



%{--</div>--}%
%{--</div> <!-- /container -->--}%
