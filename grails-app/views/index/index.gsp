<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <g:render template="title"/>
    <meta name='layout' content='main'/>
    <g:if test="${mode != null}">
        <link rel="canonical" href="${grailsApplication.config.grails.serverURL}/?mode=${mode}" />
    </g:if>
    <g:else>
        <link rel="canonical" href="${grailsApplication.config.grails.serverURL}" />
    </g:else>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35926990-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>
<g:if test="${!fasonMode}">
    <g:render template="index-can"></g:render>
</g:if>
<g:else>
    <g:render template="index-fason"></g:render>
</g:else>

</body>
</html>