<footer>
    <div class="container">
        <!-- COPYRIGHTS -->
        <p class="pull-left">canthefason.com - ${text}</p>
        <p class="pull-right"><a href="#">Back to top</a></p>
    </div>
</footer>