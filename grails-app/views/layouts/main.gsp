<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" dir="ltr"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en" dir="ltr"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><g:layoutTitle default="Can the Fason"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Can Yucel Personal Web Page">
    <meta name="keywords" content="MongoDB, PHP, Zend Framework, Grails, Java, Git, Omnet++, C++, Qt, RabbitMQ,
    Solr,Hibernate,Doctrine, Software Design Patterns, MVC, Continuous Integration, Jenkins, Scala, Vocalist, Guitarist,
    Muppet, Snowboard, Çipetpet, Neşeli Günler Kumpanyası, Shuffle, Koro, Choir, KoroSU, Sabancı Universitesi,
    Sabancı University, Ege University, Universidade do Porto, n11.com, playstore, Numara 11, Keyfimoda, Modajenik, DATAS,
    Guzel Insan, Erasmus, Istanbul, Argefar, Goller Cepte">

    <meta property="og:title" content="Can the Fason"/>
    <meta property="og:image" content="${resource(dir: 'images', file: 'can-efendi.png')}"/>
    <meta property="og:site_name" content="Can the Fason"/>
    <meta property="og:description" content="Agile Software Developer/Singing & Guitar Playing Muppet"/>


    <!-- Le styles -->
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-responsive.min.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'manual.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'typica.css')}" type="text/css">
    <link rel="stylesheet" href='${resource(dir: "js/galleria/themes/folio", file: "galleria.folio.css")}' type="text/css">

    %{--<g:javascript library="setup"/>--}%

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <g:javascript src="jquery.js"/>
    <g:javascript src="bootstrap.js"/>
    <g:javascript src="setup.js"/>
    <g:javascript src="plugins.js"/>
    <g:javascript src="galleria/galleria-1.2.9.js"/>

    <g:layoutHead/>
    <r:layoutResources />


    <!-- Le favicon -->
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

</head>

<body>

<g:include controller="index" action="topBar"></g:include>

<g:layoutBody/>
<r:layoutResources />


<g:include controller="index" action="footer"></g:include>


<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


</body>
</html>
