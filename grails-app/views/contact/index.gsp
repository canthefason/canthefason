<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:render template="../index/title"/>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35926990-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>

%{--<iframe class="hidden-phone" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;ll=37.0625,-95.677068&amp;spn=40.052282,86.572266&amp;t=m&amp;z=4&amp;output=embed"></iframe>--}%
<iframe class="hidden-phone" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Cafera%C4%9Fa+Mh.,+Istanbul,+Turkey&amp;aq=&amp;sll=40.992696,29.001287&amp;sspn=0.037705,0.090723&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Cafera%C4%9Fa+Mh.,+Kad%C4%B1k%C3%B6y%2FIstanbul+Province,+Turkey&amp;z=14&amp;iwloc=A&amp;ll=40.985121,29.02488&amp;output=embed"></iframe>

<div class="container">

    <div class="row">
        <div class="span12">
            <div class="row">
                <div class="span4 contact-info">
                    <div class="page-header">
                        <h2>Contact me</h2>
                        <p></p>
                    </div>
                    <h4>Social Connection</h4>
                    <div class="member-social">
                        <a href="<g:message code="contact.facebook"/>">
                            <i class="elegant-icon-facebook"></i>
                        </a>
                        <a href="<g:message code="contact.linkedin"/>">
                            <i class="elegant-icon-linkedin"></i>
                        </a>
                        <a href="<g:message code="contact.twitter"/>">
                            <i class="elegant-icon-twitter"></i>
                        </a>
                        <a href="<g:message code="contact.instagram"/>">
                            <i class="elegant-icon-instagram"></i>
                        </a>
                        <a href="<g:message code="contact.youtube"/>">
                            <i class="elegant-icon-youtube"></i>
                        </a>
                        <a href="<g:message code="contact.myspace"/>">
                            <i class="elegant-icon-myspace"></i>
                        </a>
                    </div>
                    <hr>
                    <h4>Contact via email</h4>
                    <p><a href="mailto:can.yucel@gmail.com"><i class="icon-envelope"></i> can.yucel@gmail.com</a></p>
                    <hr>
                    <h4>Call me</h4>
                    <p>+90 (530) 419 84 82</p>
                </div>
                <div class="span8">
                    <div class="page-header center">
                        <h2>Contact form</h2>
                        <p></p>
                        <p></p>
                    </div>
                    <g:form action="post" controller="contact"  class="">
                        <label for="fullname"><p>Your name</p></label>
                        <input type="text" name="fullname" id="fullname" value="${command?.fullname}">
                        <g:eachError var="err" bean="${command}" field="fullname">
                            <span class="text-error"><g:message error="${err}" /></span>
                        </g:eachError>
                        <label for="email"><p>Your email</p></label>
                        <input type="text" id="email" name="email" value="${command?.email}">
                        <g:eachError var="err" bean="${command}" field="email">
                            <span class="text-error"><g:message error="${err}" /></span>
                        </g:eachError>
                        <label for="message"><p>Your message</p></label>
                        <textarea name="message" id="message" cols="30" rows="10" class="span4" placeholder="Type here..." value="${command?.message}"></textarea>
                        <g:eachError var="err" bean="${command}" field="message">
                            <span class="text-error"><g:message error="${err}" /></span>
                        </g:eachError>
                        <br>
                        <button type="submit" class="btn btn-success">Send</button>
                        <g:if test="${flash.message}">
                            <span class='text-success'>*${flash.message}</span>
                        </g:if>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

</div>


</body>
</html>