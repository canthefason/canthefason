
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <g:render template="../index/title"/>
    <meta name='layout' content='main'/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'typica.css')}" type="text/css">
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-35926990-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>
    <div class="hero-unit">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <h1><strong class="pink">${title}</strong></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="galleria" style="height: 400px; background: #000">
            <g:if test="${dataType == 'picture' }">
                <a href="${resource(dir: "images/fason/cipetpet", file: "ilk-afis.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "ilk-afis.jpg")}' data-title="First Poster" data-description="Çipetpet @Pendor 20/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "ilk-konser.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "ilk-konser.jpg")}' data-title="First Çipetpet Gig" data-description="Çipetpet @Garajistanbul 12/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "look-at-me.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "look-at-me.jpg")}' data-title="Look at me" data-description="Çipetpet @Nina 23/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "sirketli.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "sirketli.jpg")}' data-title="First Competition" data-description="Çipetpet @Garajistanbul 12/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "soundcheck-nina.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "soundcheck-nina.jpg")}' data-title="Soundcheck" data-description="Çipetpet @Nina 23/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "vikvik.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "vikvik.jpg")}' data-title="Don't Come" data-description="Çipetpet @Garajistanbul 12/02/2013"/></a>
                <a href="${resource(dir: "images/fason/cipetpet", file: "yaka-karti.jpg")}"><img src='${resource(dir: "images/fason/cipetpet-thumb", file: "yaka-karti.jpg")}' data-title="Badges" data-description="Çipetpet @Garajistanbul 12/02/2013"/></a>

                <a href="${resource(dir: "images/fason/other", file: "fluting.jpg")}"><img src='${resource(dir: "images/fason/other-thumb", file: "fluting.jpg")}' data-title="Flute" data-description="@Fethiye Art Camp 2009"/></a>
                <a href="${resource(dir: "images/fason/other", file: "hmfs.jpg")}"><img src='${resource(dir: "images/fason/other-thumb", file: "hmfs.jpg")}' data-title="Hmfs" data-description="@Sabancı Studio - 2009"/></a>
                <a href="${resource(dir: "images/fason/other", file: "mezunlu.jpg")}"><img src='${resource(dir: "images/fason/other-thumb", file: "mezunlu.jpg")}' data-title="Graduate" data-description="@Sabancı University 26/06/2009"/></a>
                <a href="${resource(dir: "images/fason/other", file: "evde.jpg")}"><img src='${resource(dir: "images/fason/other-thumb", file: "evde.jpg")}' data-title="Home" data-description="@Home 2011"/></a>
                <a href="${resource(dir: "images/fason/other", file: "que-sera-sera.jpg")}"><img src='${resource(dir: "images/fason/other-thumb", file: "que-sera-sera.jpg")}' data-title="When I was just a little boy" data-description="@High School 2002"/></a>

                <a href="${resource(dir: "images/fason/neseli", file: "davullu.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "davullu.jpg")}' data-title="Beat it" data-description="Neşeli Günler Kumpanyası @Hayal Kahvesi Bistro 31/12/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "duldul.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "duldul.jpg")}' data-title="Tosun Düldül" data-description="Neşeli Günler Kumpanyası @Sabancı University 05/11/2008"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "ege.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "ege.jpg")}' data-title="Ege" data-description="Neşeli Günler Kumpanyası @Ege University 09/05/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "gitarli.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "gitarli.jpg")}' data-title="Guitar Duet" data-description="Neşeli Günler Kumpanyası @Ege University 09/05/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "grupcana.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "grupcana.jpg")}' data-title="Beginning" data-description="Neşeli Günler Kumpanyası @Sabancı University 05/11/2008"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "hursid.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "hursid.jpg")}' data-title="Hursid" data-description="Neşeli Günler Kumpanyası @Sabancı Studio 2008"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "kirmizi.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "kirmizi.jpg")}' data-title="Red" data-description="Neşeli Günler Kumpanyası @Hayal Kahvesi Bistro 31/12/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "oturan-solist.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "oturan-solist.jpg")}' data-title="Sitting in the back" data-description="Neşeli Günler Kumpanyası @Hayal Kahvesi Bistro 31/12/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "sing-it-back.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "sing-it-back.jpg")}' data-title="Sing it back" data-description="Neşeli Günler Kumpanyası @Hayal Kahvesi Bistro 31/12/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "tosun-duldul.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "tosun-duldul.jpg")}' data-title="Acoustic Duldul" data-description="Neşeli Günler Kumpanyası @Sabancı University 05/11/2008"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "marshing-on.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "marshing-on.jpg")}' data-title="Marshing On" data-description="Neşeli Günler Kumpanyası @Ege University 09/05/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "vintage.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "vintage.jpg")}' data-title="Vintage" data-description="Neşeli Günler Kumpanyası @Hayal Kahvesi Bistro 10/09/2011"/></a>
                <a href="${resource(dir: "images/fason/neseli", file: "izmir.jpg")}"><img src='${resource(dir: "images/fason/neseli-thumb", file: "izmir.jpg")}' data-title="Before gig" data-description="Neşeli Günler Kumpanyası @Ege University 09/05/2011"/></a>


                <a href="${resource(dir: "images/fason/korosu", file: "firat.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "firat.jpg")}' data-title="Firat" data-description="KoroSU @Sabancı University 27/05/2009"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "green.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "green.jpg")}' data-title="Greeny" data-description="KoroSU @Sabancı University 27/05/2009"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "bass.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "bass.jpg")}' data-title="Why I do not play bass" data-description="KoroSU @Sabancı University 27/05/2009"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "wonderwall.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "wonderwall.jpg")}' data-title="Wonderwall" data-description="KoroSU @Sabancı University 27/05/2009"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "afacan.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "afacan.jpg")}' data-title="Naughty" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "single-lady.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "single-lady.jpg")}' data-title="Single Lady" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "final-countdown-solo.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "final-countdown-solo.jpg")}' data-title="Final Countdown" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "all-choir.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "all-choir.jpg")}' data-title="Rock Choir" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "maskeli.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "maskeli.jpg")}' data-title="Masked" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "gozluklu-cocuk.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "gozluklu-cocuk.jpg")}' data-title="Glassy kid" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="${resource(dir: "images/fason/korosu", file: "thriller.jpg")}"><img src='${resource(dir: "images/fason/korosu-thumb", file: "thriller.jpg")}' data-title="Thriller" data-description="KoroSU @Sabancı University 17/05/2010"/></a>

                <a href="${resource(dir: "images/fason/shuffle", file: "aybekli.jpg")}"><img src='${resource(dir: "images/fason/shuffle-thumb", file: "aybekli.jpg")}' data-title="Cezayir" data-description="Shuffle @Chez Sakman 31/05/2009"/></a>
                <a href="${resource(dir: "images/fason/shuffle", file: "sex-bomb.jpg")}"><img src='${resource(dir: "images/fason/shuffle-thumb", file: "sex-bomb.jpg")}' data-title="Sex bomb" data-description="Shuffle @Chez Sakman 31/05/2009"/></a>
                <a href="${resource(dir: "images/fason/shuffle", file: "bronx.jpg")}"><img src='${resource(dir: "images/fason/shuffle-thumb", file: "bronx.jpg")}' data-title="Bronx" data-description="Shuffle @Bronx 08/03/2010"/></a>
                <a href="${resource(dir: "images/fason/shuffle", file: "pumpkin-king.jpg")}"><img src='${resource(dir: "images/fason/shuffle-thumb", file: "pumpkin-king.jpg")}' data-title="Pumpkin King" data-description="Shuffle @Sabancı University 09/11/2009"/></a>
            %{--<img src='${resource(dir: "images/fason/other", file: "tart.jpg")}' data-title="Tart" data-description="Competition Card"/>--}%
            </g:if>
            <g:elseif test="${dataType == 'performance'}">
                <a href="http://www.youtube.com/watch?v=eNVvX5Tvt50"><span class="video" data-title="Last Nite" data-description="Çipetpet @Roxy 16/03/2013"></span></a>
                <a href="http://www.youtube.com/watch?v=fZHcOieNeA8"><span class="video" data-title="Munich" data-description="Çipetpet @Roxy 16/03/2013"></span></a>
                <a href="http://www.youtube.com/watch?v=CzdeQrFvdbc"><span class="video" data-title="When the Sun Goes Down" data-description="Çipetpet @Garajistanbul 12/02/2013"></span></a>
                <a href="http://www.youtube.com/watch?v=vq-1xuA0II8"><span class="video" data-title="Another Brick in the Wall" data-description="KoroSU @Sabancı University 17/05/2010"></span></a>
                <a href="http://www.youtube.com/watch?v=XzV2rAOhCmU"><span class="video" data-title="Final Countdown" data-description="KoroSU @Sabancı University 17/05/2010"/></a>
                <a href="http://www.youtube.com/watch?v=peMzmgqDGa4"><span class="video" data-title="Empty Chairs at Empty Tables" data-description="@Avant Garde Hotel 11/12/2010"/></a>
                <a href="http://www.youtube.com/watch?v=1lc-OPS7a8A"><span class="video" data-title="My Way" data-description="@Avant Garde Hotel 11/12/2010"/></a>
                <a href="http://www.youtube.com/watch?v=ll2vHm9Txzg"><span class="video" data-title="Sex Bomb" data-description="Shuffle @Chez Sakman 31/05/2009"/></a>

            </g:elseif>
        </div>

    </div>

    <g:javascript>
        Galleria.loadTheme('${resource(dir: "js/galleria/themes/folio", file: "galleria.folio.min.js")}');
        Galleria.run('#galleria');
    </g:javascript>
</body>